import "./App.css";
import TableComponents from "./components/table/TableComponent";

function App() {
  return (
    <div className="App">
      <TableComponents />
    </div>
  );
}

export default App;
