import { Button, Form, Input, Modal } from "antd";

interface Props {
  isModalOpen: boolean;
  handleOk?: (e: any) => void;
  handleCancel?: () => void;
}

const ModalComponent = (props: Props) => {
  return (
    <div>
      <Modal
        title="From"
        open={props.isModalOpen}
        onCancel={props.handleCancel}
        footer={false}
      >
        <Form name="form_item_path" layout="vertical" onFinish={props.handleOk}>
          <Form.Item
            label="first name"
            name="firstName"
            rules={[{ required: true }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="last name"
            name="lastName"
            rules={[{ required: true }]}
          >
            <Input />
          </Form.Item>
          <Form.Item label="Age" name="Age" rules={[{ required: true }]}>
            <Input type="number" />
          </Form.Item>
          <Form.Item label="Email" name="email" rules={[{ required: true }]}>
            <Input type="email" />
          </Form.Item>
          <Form.Item
            label="Address"
            name="address"
            rules={[{ required: true }]}
          >
            <Input.TextArea />
          </Form.Item>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
          <Button className="ml-1" onClick={props.handleCancel}>
            cancel
          </Button>
        </Form>
      </Modal>
    </div>
  );
};

export default ModalComponent;
