import { Button, Card, Table } from "antd";
import { PlusOutlined, DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { useState } from "react";
import ModalComponent from "../ModalComponent";
import ModalUpdateComponent from "../ModalUpdateComponent";
import ModalDeleteComponent from "../ModalDeleteComponent";

const TableComponents = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isOpenUpdate, setIsOpenUpdate] = useState(false);
  const [dataUpdate, setDataUpdate] = useState();
  const [deletes, setDeletes] = useState(false);
  const columns = [
    {
      title: "First Name",
      dataIndex: "firstName",
      key: "firstName",
    },
    {
      title: "Last Name",
      dataIndex: "lastName",
      key: "lastName",
    },
    {
      title: "Age",
      dataIndex: "Age",
      key: "age",
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
    },
    {
      title: "Address",
      dataIndex: "address",
      key: "address",
    },
    {
      title: "Action",
      dataIndex: "",
      key: "x",
      render: (e: any) =>
        userData && (
          <div>
            <EditOutlined
              onClick={() => onOpenUpdate(e)}
              style={{ cursor: "pointer" }}
            />
            <DeleteOutlined
              onClick={onDelete}
              style={{ color: "red", cursor: "pointer", marginLeft: "10px" }}
            />
          </div>
        ),
    },
  ];

  function readData(key: any) {
    const data = localStorage.getItem(key);
    return data ? JSON.parse(data) : null;
  }

  const userData = readData("user1");

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = (values: any) => {
    createData("user1", values);
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  function createData(key: any, value: any) {
    localStorage.setItem(key, JSON.stringify(value));
  }

  const onUpdate = (e: any) => {
    updateData("user1", e);
    setIsOpenUpdate(false);
  };

  const onOpenUpdate = (e: any) => {
    setDataUpdate(e);
    setIsOpenUpdate(true);
  };

  const onCancelUpdate = () => {
    setIsOpenUpdate(false);
  };

  const onDelete = () => {
    setDeletes(true);
  };

  const onFirmDelete = () => {
    localStorage.removeItem("user1");
    setDeletes(false);
  };

  const onCancelDelete = () => {
    setDeletes(false);
  };

  function updateData(key: any, updatedValue: any) {
    const existingData = readData(key);
    if (existingData) {
      const newData = { ...existingData, ...updatedValue };
      localStorage.setItem(key, JSON.stringify(newData));
      return true;
    }
    return false;
  }

  return (
    <div style={{ padding: "20px" }}>
      <div className="d-flex justify-content-end py-4">
        <Button
          size="large"
          onClick={showModal}
          style={{ backgroundColor: "green", color: "#fff" }}
        >
          <div>
            <PlusOutlined />
            <span className="ml-1">Create</span>
          </div>
        </Button>
      </div>
      <Card className="shadow-lg h-100">
        <Table
          scroll={{ x: 1000 }}
          dataSource={[userData]}
          columns={columns}
          pagination={false}
        />
      </Card>
      <ModalComponent
        isModalOpen={isModalOpen}
        handleOk={handleOk}
        handleCancel={handleCancel}
      />
      <ModalUpdateComponent
        isOpenUpdate={isOpenUpdate}
        onUpdate={onUpdate}
        onCancelUpdate={onCancelUpdate}
        dataUpdate={dataUpdate}
      />
      <ModalDeleteComponent
        deletes={deletes}
        onFirmDelete={onFirmDelete}
        onCancelDelete={onCancelDelete}
      />
    </div>
  );
};

export default TableComponents;
