import { Button, Form, Input, Modal } from "antd";
interface Props {
  isOpenUpdate: boolean;
  onUpdate?: (e: any) => void;
  onCancelUpdate?: () => void;
  dataUpdate: any;
}
const ModalUpdateComponent = (props: Props) => {
  console.log("props", props.dataUpdate?.firstName);

  return (
    <Modal
      title="From"
      open={props.isOpenUpdate}
      onCancel={props.onCancelUpdate}
      footer={false}
    >
      <Form name="form_item_path" layout="vertical" onFinish={props.onUpdate}>
        <Form.Item
          label="first name"
          name="firstName"
          rules={[{ required: true }]}
        >
          <Input defaultValue={props.dataUpdate?.firstName} />
        </Form.Item>
        <Form.Item
          label="last name"
          name="lastName"
          rules={[{ required: true }]}
        >
          <Input defaultValue={props.dataUpdate?.lastName} />
        </Form.Item>
        <Form.Item label="Age" name="Age" rules={[{ required: true }]}>
          <Input type="number" defaultValue={props.dataUpdate?.Age} />
        </Form.Item>
        <Form.Item label="Email" name="email" rules={[{ required: true }]}>
          <Input type="email" defaultValue={props.dataUpdate?.email} />
        </Form.Item>
        <Form.Item label="Address" name="address" rules={[{ required: true }]}>
          <Input.TextArea defaultValue={props.dataUpdate?.address} />
        </Form.Item>
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
        <Button className="ml-1" onClick={props.onCancelUpdate}>
          cancel
        </Button>
      </Form>
    </Modal>
  );
};
export default ModalUpdateComponent;
