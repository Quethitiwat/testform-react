import { Button, Modal } from "antd";

interface Props {
  deletes: boolean;
  onFirmDelete?: (e: any) => void;
  onCancelDelete?: () => void;
}

const ModalDeleteComponent = (props: Props) => {
  return (
    <Modal open={props.deletes} onCancel={props.onCancelDelete} footer={false}>
      <div className="p-4 d-flex justify-content-center">
        <h2>ยืนยันจะลบข้อมูลหรือไม่ ?</h2>
      </div>
      <div className="d-flex justify-content-center">
        <Button
          onClick={props.onFirmDelete}
          style={{ color: "#fff", backgroundColor: "red" }}
          type="primary"
          htmlType="submit"
        >
          ยืนยัน
        </Button>
        <Button onClick={props.onCancelDelete} className="ml-3">
          ยกเลิก
        </Button>
      </div>
    </Modal>
  );
};

export default ModalDeleteComponent;
